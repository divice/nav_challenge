import numpy as np
import random
import asyncio
from functools import reduce

def get_avg_value(s):
    return  int(reduce(lambda x, y: x + y, s) / len(s))

def white_noise_generator(t_len= 0.05, freq =1000.0, dt = 0.0001):
    # set the see for reproducible results
    rng = np.random.RandomState(0)

    # create the random values to interpolate between
    assert dt < freq
    white_size = int(t_len * freq)
    white_vals = np.random.uniform(low=0, high=1024, size=white_size)

    # do the interpolation
    tot_size = int(t_len / dt)
    step_size = int(1 / freq / dt)
    n_shape = (white_size, step_size)
    white_noise = (white_vals[:, None] * np.ones(n_shape)).reshape(tot_size)

    assert white_vals.shape[0] < white_noise.shape[0]
    white_noise = white_noise.astype(int)

    return white_noise.tolist()