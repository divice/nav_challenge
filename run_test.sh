#! /bin/bash
echo "Starting Server and clients..."
source ./venv/bin/activate
python web_server_counter.py & python web_client_simulation.py 
echo "Finished. Find results in tests/out"
