#! /bin/bash
echo "Installing virtualenv" 
python3 -m pip install virtualenv
#rm -R -f venv
virtualenv -p python3 venv
source ./venv/bin/activate
echo "Python version is: "
python -V
echo "Installing dependencies"
python setup.py install
echo "Compiling cython module..." 
python navenio/compile.py build_ext --inplace
chmod +x run_test.sh
 
