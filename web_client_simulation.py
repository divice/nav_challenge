import os
import time
import json
import time
import multiprocessing
import random
from datetime import datetime

import aiohttp
import asyncio

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from tests.generate_signal import white_noise_generator, get_avg_value

url = 'http://127.0.0.1:9001/get_sum_of_crossing_events'

# async def got_to_sleep_please(seconds):
#     await asyncio.sleep(seconds)

def plot_signal(signal, threshold, count):
    # # plot the result to file for visual verificaition 
    plt.clf()
    plt.figure(figsize=(20,10))
    plt.axhline(y=get_avg_value(signal), color='grey', linestyle='--')
    plt.axhline(y=threshold, color='r', linestyle='-')
    red_patch = mpatches.Patch(color='red', label='Threshold value')
    grey_patch = mpatches.Patch(color='grey', label='Average value')
    count_patch = mpatches.Patch(color='k', label = 'Total Crossing Points: {}'.format(count))
    plt.legend(handles=[red_patch,grey_patch,count_patch])
    plt.plot(signal)
    #plt.show()
    filename = 'signal_example_at_{}.png'.format( str(datetime.now())[:-7] )
    plt.savefig(os.path.join('tests','out',filename), format='png')


async def login(session, url = 'http://127.0.0.1:9001/login'):
    
    headers = {"Authorization": 'd998dddf-269d-443e-93cb-9c9e8845d927'}    
    data = { 'user': 'user', 'password': 'user_password' }
    
    resp = await session.post(url, data={'data':json.dumps(data)}, headers=headers)
    if resp.status is not 200:
        return False
    return True


async def request_forever():
    counter = 0

    while 1:

        signal = white_noise_generator()
        threshold = random.randint(1,1024) 

        data = {
                'threshold':threshold, 'signal': json.dumps(signal)
                 }    
        headers = {"Authorization": 'd998dddf-269d-443e-93cb-9c9e8845d927'}
        
        async with aiohttp.ClientSession() as session:    
            resp = await session.post(url, data={'data':json.dumps(data)}, headers=headers)
            if resp.status is not 200:
                if not await login(session):
                    return 'LOGIN ERROR'

            response = await resp.text()        
            #print (response)
            if response:
                count = int(response)
       
        counter +=1
        if counter == 500:
            counter = 0
            plot_signal(signal,threshold, count)
        #await got_to_sleep_please(0.001)



def init_client():
    loop = asyncio.get_event_loop()
    loop.run_until_complete( request_forever() )


exitFlag = 0
for process_id in range(1,multiprocessing.cpu_count()):
    process = multiprocessing.Process(target=init_client, args=())
    os.system("taskset -p -c %d %d" % (process_id % multiprocessing.cpu_count(), os.getpid()))
    process.start()


