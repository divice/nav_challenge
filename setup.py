import os
import re
import sys

from setuptools import find_packages, setup


if sys.version_info.major < 3 or sys.version_info.minor <5 or sys.version_info.minor <3 :
    print('Python version must be > 3.5.3 !')
    sys.exit()



install_requires = [
    'cython',    
    'aiohttp==3.4.4 ',
    'py-bcrypt',
    'matplotlib',
    'numpy',
]


setup(name='navenio',
      version='0.1',
      description='navion challenge',
      platforms=['POSIX'],
      packages=find_packages(),
      include_package_data=True,
      install_requires=install_requires,
      zip_safe=False)
