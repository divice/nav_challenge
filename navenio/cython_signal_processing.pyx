from functools import reduce
import sys
import asyncio


def get_avg_value(s):
    return  int(reduce(lambda x, y: x + y, s) / len(s))

@asyncio.coroutine
def cython_get_sum_of_crossing_events(signal, threshold=0):
    
    # check all values are int ?
    if not signal:
        return

    if not threshold:
        threshold = get_avg_value(signal)
  
    down_flag = False
    up_flag = False
    count = 0

    if signal[0] < threshold:
        down_flag = True
    elif signal[0] > threshold:
        up_flag = True
    else:
        equal_flag = True

    for i in range(0, len(signal)-1):

        if signal[i] < threshold:
            if signal[i+1] > threshold:
                count += 1
                continue
            elif signal[i+1] == threshold:
                down_flag = True
                up_flag = False
                continue

        elif signal[i] > threshold:
            if signal[i+1] < threshold:
                count += 1
                continue
            elif signal[i+1] == threshold:
                up_flag = True
                down_flag = False
                continue
        else:
            if signal[i+1] < threshold:
                if up_flag:
                    count +=1
                    continue  
            if signal[i+1] > threshold:    
                if down_flag:
                    count +=1
                    continue  
    return count 