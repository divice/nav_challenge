import bcrypt
import uuid
import asyncio

from datetime import datetime  

LIST_OF_KNOWN_DEVICES = [ '{}'.format(uuid.uuid4()) for x in range(1000) ] + ['d998dddf-269d-443e-93cb-9c9e8845d927']

USERS_LIST = ['user', 'user_A', 'user_B']


def get_hashed_password(plain_text_password):
    return bcrypt.hashpw(plain_text_password, bcrypt.gensalt())

HASHES_LIST = [ get_hashed_password('user_password'.encode('utf-8')),
                get_hashed_password('user_A_password'.encode('utf-8')),
                get_hashed_password('user_B_password'.encode('utf-8'))]


#tuple of uuid and username (uuid, logged_time)
active_devices_list = []

def check_password(plain_text_password, hashed_password):
    return bcrypt.checkpw(plain_text_password, hashed_password)


async def user_is_authorized(user, passw):
    if user and passw:
        try:
            return check_password(passw.encode('utf-8'), HASHES_LIST[USERS_LIST.index(user)])
        except:
            return False
    else:
        return False

async def remember_device(device_id):
    global active_devices_list
    if device_id in LIST_OF_KNOWN_DEVICES:        
        active_devices_list += [(device_id, datetime.now())]
        return True
    else: 
        return False

async def forget_device(device_id):
    global active_devices_list 
    previous_len = len(active_devices_list) 
    active_devices_list = [x for x in active_devices_list if x[0] != device_id]
    return True if previous_len < len(active_devices_list) else False

async def check_device(device_id):
    global active_devices_list
    return True if device_id in ([device_id for x in active_devices_list if x[0] == device_id]) else False  


async def valid_uuid(device_id):
    # TODO: uuid validation
    return True


async def LogginTimeout(scan_seconds, timeout_seconds):
    #service for removing devices from list after timeout
    global active_devices_list
    while 1:
        await asyncio.sleep(scan_seconds)
        now = datetime.now()
        active_devices_list = [x for x in active_devices_list if (now - x[1]).total_seconds() > timeout_seconds]



