from functools import reduce
import asyncio

def get_avg_value(s):
    return  int(reduce(lambda x, y: x + y, s) / len(s))

@asyncio.coroutine
def get_sum_of_crossing_events(signal, threshold=0):
    '''API Function: return all crossing events for threshold value
    of signal

    @param:
    singal - list of ints
    threshold - int

    @return:
    count - int

    @notes
    It has been optimized to continue looping each times it finds a condition
    skipping all next if statements for that value. 
    '''
    
    # check all values are int ?
    if not signal:
        return

    if not threshold:
        threshold = get_avg_value(signal)
  
    down_flag = False
    up_flag = False
    count = 0

    if signal[0] < threshold:
        down_flag = True
    elif signal[0] > threshold:
        up_flag = True
    else:
        equal_flag = True

    for i in range(0, len(signal)-1):

        if signal[i] < threshold:
            if signal[i+1] > threshold:
                count += 1
                continue
            elif signal[i+1] == threshold:
                down_flag = True
                up_flag = False
                continue

        elif signal[i] > threshold:
            if signal[i+1] < threshold:
                count += 1
                continue
            elif signal[i+1] == threshold:
                up_flag = True
                down_flag = False
                continue
        else:
            if signal[i+1] < threshold:
                if up_flag:
                    count +=1
                    continue  
            if signal[i+1] > threshold:    
                if down_flag:
                    count +=1
                    continue  
    return count 

