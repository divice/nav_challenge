import asyncio
import json
#import ujson

from aiohttp import web
from aiohttp import WSCloseCode

import settings

from auth import user_is_authorized, check_device, remember_device, check_device, valid_uuid, LogginTimeout

from signal_processing import get_sum_of_crossing_events
from cython_signal_processing import cython_get_sum_of_crossing_events


async def handler_root(request):
    
    device_is_logged = request.headers.get('Authorization','')    
    if device_is_logged:
        device_is_logged = await check_device(device_is_logged)

    if not device_is_logged:
        return web.Response(text=settings.LOGIN_MESSAGE) 

    return web.Response(text = settings.WELCOME_MESSAGE if device_is_logged else settings.LOGIN_MESSAGE,
                        content_type='text/html')




async def handler_login(request):
    
    device_id = request.headers.get('Authorization','')
    if not await valid_uuid(device_id):
        return web.Response(status=403)  

    if request.method == 'POST':
        auth = await request.post()            
        auth = auth.get('data','')
        if auth:
            auth = json.loads(auth)
            user = auth.get('user','')
            passw = auth.get('password','')
        else:
            web.Response(status=401)           

    if await user_is_authorized(user, passw):
        if await remember_device(device_id):
            raise web.HTTPFound('/')

    return web.Response(status=401)




async def handler_logout(request):
    
    device_id = request.headers.get('Authorization','')    
    if await valid_uuid(device_id):
        device_id = await check_device(device_id)
    
    if not device_id:
        return web.Response(status=403) 

    if await forget_device(device_id):
        raise web.HTTPFound('/')
    else:
        return web.Response(status=401)




async def handler_get_sum_of_crossing_events(request):

    device_id = request.headers.get('Authorization','')    
    if await valid_uuid(device_id):
        device_id = await check_device(device_id)
    
    if not device_id:
        return web.Response(status=403) 

    if request.method == 'POST':
        data = await request.post()
        data = data['data']
        data = json.loads(data)

        # event specific data
        threshold = int(data.get('threshold','0'))
        signal = json.loads(data.get('signal',[]))

        if settings.CYTHON_ON:
            count = await cython_get_sum_of_crossing_events(signal, threshold)
        else:    
            count = await get_sum_of_crossing_events(signal, threshold)
        return web.Response(text=str(count))




async def make_app():

    app = web.Application()

    # add the routes
    app.add_routes([
        web.get('/', handler_root),
        web.post('/login', handler_login),      
        web.post('/get_sum_of_crossing_events', handler_get_sum_of_crossing_events)])

    #start timeout service
    asyncio.ensure_future(LogginTimeout(settings.SERVICE_TIMEOUT_SCAN_SECONDS, settings.TIMEOUT_SECONDS))
    
    return app

if __name__ == '__main__':
    web.run_app(make_app(), port=settings.PORT)


