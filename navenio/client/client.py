import time
import aiohttp
import asyncio
import json
import sys 
import itertools

url = 'http://127.0.0.1:9001/get_sum_of_crossing_events'

async def login(session, uuid, user, password, url = 'http://127.0.0.1:9001/login'):
    
    headers = {"Authorization": uuid}    
    data = { 'user': user, 'password': password }
    print (headers)
    resp = await session.post(url, data={'data':json.dumps(data)}, headers=headers)
    if resp.status is not 200:
        return False
    return True


async def main():

    data = {}
    headers = {}
    with open("client_settings.json") as client_settings:
        settings = json.load(client_settings)
        data = settings.get('data',{})
        headers = settings.get('header',{}) 
        print (headers)
    async with aiohttp.ClientSession() as session:    
        resp = await session.post(url, data={'data':json.dumps(data)}, headers=headers)
        if resp.status is not 200:
            if not await login(session, headers.get('Authorization',''), data.get('user',''), data.get('password','')):
                print ('LOGIN ERROR')
                return

        count = await resp.text()        

        print('Count of crossing events: _{}_ for threshold: _{}_'.format(count, data.get('threshold') ))

if __name__ == '__main__':    
    loop = asyncio.get_event_loop()
    loop.run_until_complete( main() )
    loop.close()

