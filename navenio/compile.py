from distutils.extension import Extension

from Cython.Distutils import build_ext

from distutils.core import setup

ext_modules = [
    Extension("navenio/cython_signal_processing",  ["navenio/cython_signal_processing.pyx"]),
]

setup(
    name = 'cython_signal_processing',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules
)
