# nav_challenge

**********************
Navenio challenge
**********************

![Alt text](tests/out/signal_example.png?raw=true "Signal example")


============
Introduction
============

I have taken some assumptions in order to give the solution a little bit of context.

There are many IoT devices targeting an API with different UUIDs associated with one user account which pays for device and those must be registered.

============
Architecture
============

The architecture for this solution is 'event-driven' to handle the problem of concurrency in asynchronous/non-blocking events associated to handlers all running in a cooperative event-loop by dispatching/awaiting events mechanism.

==============
Implementation
==============

To implement this architecture the weapon of choice is the aiohttp_ framework written in python taking advantages of the new asyncio library that's has been incorporated since version 3.5

The authentication is achieved by simple HTTP basic/digest using also custom HTTP Header for device identification. Passwords on server side are stored hashed. 

===========
Scalability
===========
Concurrent scalability can be achieved by running multiple instances of web_server.py (one per core) on different ports. Clients can be configure by groups to point at each port or other strategies might also be implemented.

The algorithm to calculate crossing points events can also be optimized if signals share some pattern meaning signals ar not 100%  stochastic. I have included a cythonized version of the algorithm that has to be compile on running Hardware. I have mesuared a +30% speed increase by using compiled version of the same algorithm with cython against pyc-bytecode (cpyhon.)

This +30% speed increase represented a +10% increased in web_server.py performance.

It might be worth trying Pypy or perhaps other framworks or even languages if performance is he most important issue. NodeJS ?

Also for highers throughput out websockets should be considered (supported out of the box by aiohttp).



==========
References
==========

https://berb.github.io/diploma-thesis/original/ 
 
https://www.techempower.com/benchmarks/ 
 
https://morepypy.blogspot.com/2017/03/async-http-benchmarks-on-pypy3.html 
 
https://docs.aiohttp.org/ 

http://www.gevent.org/ 
 
https://medium.com/@mihaigeorge.c/web-rest-api-benchmark-on-a-real-life-application-ebb743a5d7a3 
 
http://klen.github.io/py-frameworks-bench/ 



***************
Installation
***************

===============
Ubuntu / Debian
===============

Install the app::

    $ git clone https://divice@bitbucket.org/divice/nav_challenge.git
    $ chmod +x install.sh
    $ ./install.sh

Run tests::

    $ ./run_test.sh

Open browser to check server::

    http://127.0.0.1:9001


==============
Fedora
==============

...

Requirements
============
* aiohttp_
* pybcrypt_
* numpy_
* matplotlib_
* cython_

.. _aiohttp: https://github.com/aio-libs/aiohttp
.. _cython: http://cython.org/
.. _pybcrypt: https://pypi.org/project/py-bcrypt/
.. _numpy: http://www.numpy.org/
.. _matplotlib: https://matplotlib.org/
